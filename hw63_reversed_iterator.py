"""Homework 6 Task 3 implementation."""

from typing import Any


class ReversedIterator:
    """Iterator that returns elements reversed."""

    def __init__(self, items: list) -> None:
        self.items = items
        self.index = len(self.items) - 1

    def __next__(self) -> Any:
        if self.index < 0:
            raise StopIteration
        return_value = self.items[self.index]
        self.index -= 1

        return return_value

    def __iter__(self):
        return self


if __name__ == '__main__':

    l1 = list(range(5))
    print(l1)

    for item in ReversedIterator(l1):
        print(item)

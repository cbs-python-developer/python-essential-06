"""Homework 6 Task 1 implementation."""


# Create an iterable
numbers = [1, 2, 3, 4, 5]

print('\n----FOR LOOP')
for number in numbers:
    print(number)

print('\n----ITERATOR')
iterator_obj = iter(numbers)
while True:
    try:
        item = next(iterator_obj)
        print(item)
    except StopIteration:
        break

"""
Пример реализации списка с итератором
"""
from typing import Any, Optional


class MyList(object):
    """Класс списка"""

    class _ListNode(object):
        """Внутренний класс элемента списка"""

        # По умолчанию атрибуты-данные хранятся в словаре __dict__.
        # Если возможность динамически добавлять новые атрибуты
        # не требуется, можно заранее их описать, что более
        # эффективно с точки зрения памяти и быстродействия, что
        # особенно важно, когда создаётся множество экземляров
        # данного класса.
        __slots__ = ('value', 'prev', 'next')

        def __init__(self, value, prev=None, next=None):
            self.value = value
            self.prev = prev
            self.next = next

        def __repr__(self):
            return 'MyList._ListNode({}, {}, {})'.format(self.value, id(self.prev), id(self.next))

    class _Iterator(object):
        """Внутренний класс итератора"""

        def __init__(self, list_instance):
            self._list_instance = list_instance
            self._next_node = list_instance._head

        def __iter__(self):
            return self

        def __next__(self):
            if self._next_node is None:
                raise StopIteration

            value = self._next_node.value
            self._next_node = self._next_node.next

            return value

    def __init__(self, iterable=None):
        # Длина списка
        self._length = 0
        # Первый элемент списка
        self._head = None
        # Последний элемент списка
        self._tail = None

        # Добавление всех переданных элементов
        if iterable is not None:
            for element in iterable:
                self.append(element)

    def append(self, element):
        """Добавление элемента в конец списка"""

        # Создание элемента списка
        node = MyList._ListNode(element)

        if self._tail is None:
            # Список пока пустой
            self._head = self._tail = node
        else:
            # Добавление элемента
            self._tail.next = node
            node.prev = self._tail
            self._tail = node

        self._length += 1

    def clear(self) -> None:
        """Clear the list."""

        self._length = 0
        self._head = None
        self._tail = None

    def insert(self, index: int, element: Any) -> None:
        """Insert a new element in the list before index position."""

        index_node = self.__find_node(index)
        new_node = MyList._ListNode(element)

        new_node.next = index_node
        new_node.prev = index_node.prev
        index_node.prev = new_node
        if new_node.prev is not None:
            new_node.prev.next = new_node
        else:
            self._head = new_node

        self._length += 1

    def pop(self, index: Optional[int] = None) -> Any:
        """Remove the element from the index position and returns it.
        By default, the last item is removed and returned."""

        if index is None:
            index = self._length - 1

        pop_node = self.__find_node(index)

        prev_node, next_node = pop_node.prev, pop_node.next
        try:
            prev_node.next = next_node
        except AttributeError:
            self._head = next_node
        try:
            next_node.prev = prev_node
        except AttributeError:
            self._tail = prev_node

        self._length -= 1

        return pop_node.value

    def __find_node(self, index: int) -> 'MyList._ListNode':
        """Find a node in the list by its index."""

        if not 0 <= index < len(self):
            raise IndexError('list index out of range')

        node = self._head
        for _ in range(index):
            node = node.next

        return node

    def __len__(self):
        return self._length

    def __repr__(self):
        # Метод join класса str принимает последовательность строк
        # и возвращает строку, в которой все элементы этой
        # последовательности соединены изначальной строкой.
        # Функция map применяет заданную функцию ко всем элементам последовательности.
        return 'MyList([{}])'.format(', '.join(map(repr, self)))

    def __getitem__(self, index):
        node = self.__find_node(index)
        return node.value

    def __iter__(self):
        return MyList._Iterator(self)


def main():
    # Создание списка
    my_list = MyList([1, 2, 5])

    # Вывод длины списка
    print(len(my_list))

    # Вывод самого списка
    print(my_list)

    print()

    # Обход списка
    for element in my_list:
        print(element)

    print()

    # Повторный обход списка
    for element in my_list:
        print(element)

    print("\n----Clear the list")
    my_list.clear()
    print(my_list)

    print("\n----Add elements to the list")
    print("my_list.insert(0, 1) >>> ", end='')
    try:
        my_list.insert(0, 1)
    except IndexError as err:
        print(f"IndexError: {err}")
    my_list.append(2)
    print(my_list)
    my_list.insert(0, 1)
    print(my_list)
    my_list.insert(1, 'midst')
    print(my_list)
    my_list.insert(0, 'start')
    print(my_list)

    print(f"len(my_list) >>> {len(my_list)}")
    print(f"my_list[2] >>> {my_list[2]}")

    print("\n----Removing elements from the list")
    print(f"my_list.pop() >>> {my_list.pop()}")
    print(my_list)
    print(f"my_list.pop(1) >>> {my_list.pop(1)}")
    print(my_list)
    print(f"my_list.pop(0) >>> {my_list.pop(0)}")
    print(my_list)
    print(f"my_list.pop(0) >>> {my_list.pop(0)}")
    print(my_list)
    print("my_list.pop() >>> ", end='')
    try:
        my_list.pop()
    except IndexError as err:
        print(f"IndexError: {err}")
    print(f"len(my_list) >>> {len(my_list)}")


if __name__ == '__main__':
    main()
